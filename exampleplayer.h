#ifndef __EXAMPLEPLAYER_H__
#define __EXAMPLEPLAYER_H__

#include <iostream>
#include <unordered_map>
#include "common.h"
#include "board.h"
#include <vector>

using namespace std;

class ExamplePlayer {

private:
  
  Side color;
  Side opponent;
  std::unordered_map<int, Side> players;
  Board * game;

public:
    ExamplePlayer(Side side);
    ~ExamplePlayer();
    int evaluate(Move* myMove, int side, Board* gameBoard);
    Move *doMove(Move *opponentsMove, int msLeft);

    int evaluateBoard(Board* gameBoard);
    std::vector<Node*> generateChildren(Node* node, Side side);
    int heuristic(Node * node, int counter);
    int nodeScore(Node * node, int depth);

};

#endif
