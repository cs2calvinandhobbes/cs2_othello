#ifndef __BOARD_H__
#define __BOARD_H__

#include <bitset>
#include "common.h"
using namespace std;

class Board {
   
private:
    bitset<64> black;
    bitset<64> taken;    
       
    bool occupied(int x, int y);
    bool get(Side side, int x, int y);
    void set(Side side, int x, int y);
    bool onBoard(int x, int y);
      
public:
    Board();
    ~Board();
    Board *copy();
        
    bool isDone();
    bool hasMoves(Side side);
    bool checkMove(Move *m, Side side);
    void doMove(Move *m, Side side);
    int count(Side side);
    int countBlack();
    int countWhite();
};

class Node 
{ 
    int score;
    Node* parent;
    Move * bestMove; 
    Board * gameBoard;
    Move * moveFromParent; //records how to get between the parent and child node 
public: 
    // insert constructors, destructors, 
    // accessors, and mutators here 
    Node(int score, Node* parent, Board* game, Move* move);
    ~Node();
    void setScore(int sc);
    void setBestMove(Move* moove);
    int getScore();
    Move* getMoveFromParent();
    Node* getParent();
    Move* getBestMove(); 
    Board* getBoard();
    bool hasParent();
}; 

#endif
