Division of Labor:
  WEEK 1
    - Basith created the bitbucket repository
    - Alex did much of the coding 
    - Basith tested, debugged, and modified the heuristic as needed

  WEEK 2
    - Basith cut down the amount of coding necessary by implementing a map to store the sides
    - Alex drew out and implemented the Node class and the recursive searching functionality
    - Basith tested, debugged, and modified the bot as needed


AI:
 
  - The heuristic starts by counting the number of pieces flipped
  - Valuing Special Locations:
    - Our heuristic places extreme value on corners (major plus)
    - As such, it also places a very negative score 
      on spots next to corners (major minus)

  - Predicting the future:
    - The player starts with a node representing the current board
    - It then creates nodes for the children...
    - ...and recurses to a given depth

  - Honestly, this isn't a super-advanced AI. It's pretty basic, if anything.
    That said, We don't think it will perform badly at all. 

Hoping for...above average!
