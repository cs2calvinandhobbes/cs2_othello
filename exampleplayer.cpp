#include "exampleplayer.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
ExamplePlayer::ExamplePlayer(Side side) {

  this->color = side;
  if(color == BLACK)
  {
    this->opponent = WHITE;
  }
  else
  {
    this->opponent = BLACK;
  }
  this->players[1] = color;
  this->players[-1] = opponent;
  
  this->game = new Board();

}

/*
 * Destructor for the player.
 */
ExamplePlayer::~ExamplePlayer() {
    delete game;
}


/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *ExamplePlayer::doMove(Move *opponentsMove, int msLeft) {

  // Check for opponent's move
    if (opponentsMove != NULL)
    {
        game->doMove(opponentsMove, opponent);
    }

    // Compute our move
    if (game->hasMoves(color))
    {
      // Create a root node 
       Node * head = new Node(-100000, NULL, game, NULL);
       // iterate to depth 4
       nodeScore(head, 4);

       // Get the best move
       Move * nextMove = head->getBestMove();
       // Print our move to terminal
       std::cerr << "Chose move: (" << nextMove->getX() << 
	 ", " << nextMove->getY() << ")" << std::endl; 

       // Do our move and return it
       game->doMove(nextMove, color);
       return nextMove;
    }
    
    return NULL;
}

/*
 * Evaluates a move given a board and the side making the move
 */
int ExamplePlayer::evaluate(Move *myMove, int side, Board *gameBoard)
{
  int moveScore, x, y;
  Board * gameCopy;
  
  // Count the number of opponent's pieces      
  int numTheirs = gameBoard->count(players[-side]);
  
  // Create a copy of the board
  gameCopy = gameBoard->copy();
  
  // Simulate doing the move
  gameCopy->doMove(myMove, players[side]);
  
  // Get the new number of opponent's pieces
  int newNum = gameCopy->count(players[-side]);
  
  // Get the coordinates of the move
  x = myMove->getX();
  y = myMove->getY();
  
  moveScore = numTheirs - newNum; // Setting moveScore
  
  // Reward/Punish based on placement:
  if ((x == 0 || x == 7) && (y == 0 || y == 7))
    {
      moveScore += 25; // Corners
    }
  else if ((x == 0 || x == 7) && (y > 1 && y < 6))
    {
      moveScore += 6; // (good) edges
    }
  else if ((x > 1 && x < 6) && (y == 1 || y == 7))
    {
      moveScore += 6; // (good) edges
    }
  else if ((x == 1 || x == 6) && (y == 1 || y == 6))
    {
      moveScore += -18; // kitty corner to the corner
    }
  else if ((x == 0 || x == 7) && (y == 1 && y == 6))
    {
      moveScore += -14; // edge adjacent to corner
    }
    else if ((y == 0 || y == 7) && (x == 1 && x == 6))
    {
      moveScore += -14; // edge adjacent to corner
    }

  delete gameCopy; // Cleanup
  return moveScore;
}    

// Generate the "children" of a given node and the side
std::vector<Node*> ExamplePlayer::generateChildren(Node* node, Side side)
{
    std::vector<Node*> children;
    Board* gameBoard = node->getBoard();
    for (int i = 0; i < 8; i++) // loop through rows
    {
	    for(int j = 0; j < 8; j++) // loop through columns
        {
	            
                Move *move = new Move(i, j); 
                if (gameBoard->checkMove(move, side)) //check if move is possible
                {
                    Board *newGame = gameBoard->copy(); //makes new board
                    newGame->doMove(move,side); //performs the move
                    Node * newNode = new Node(-10000, node, newGame, move); //makes node for the new move
                    children.push_back(newNode); //add node to the list
                }
        }
    }
    return children;
}

// Evaluates the score of tree "leaves" (end nodes)
int ExamplePlayer::heuristic(Node * node, int counter)
{
  //figures out whether to add or subtract the move score / who's moving
    int score = 0;
    Node * workingNode = node;
    while(workingNode->hasParent()) //while there are still scores to calculate
    {
        Move* movie = workingNode->getMoveFromParent(); //move that's being played
        workingNode = workingNode->getParent(); //helps to get the board the move's played on
        Board* boardie = workingNode->getBoard(); //gets the board the move's played on
        if(counter % 2 == 0) //means that it's a move of the opponent
        {
            score -= evaluate(movie, -1, boardie); //subtracts the opponent's gain from the board
        }
        else
        {
            score += evaluate(movie, 1, boardie); //adds our gain to the score
        }
        counter++;
    }
    return score;
}

// Computes the best-scoring move for a Node, recursively
int ExamplePlayer::nodeScore(Node* node, int depth) //WORKS FROM THE TOP DOWN
{
  std::vector<Node*> nodes;
  std::vector<Node*>::iterator i;

    if(depth == 0)
    {
      return heuristic(node, 0); //returns the score of the move if at the maximum depth
    }
    else if(depth % 2 == 0) //our move
    {

        nodes = generateChildren(node, color); //finds all the children of the node
        if(nodes.size() == 0)
        {
            return heuristic(node, 0);
        }
        else
        {
            int maxi = -10000000; //looking for the maximum score of the children nodes
            for(i = nodes.begin(); i != nodes.end(); i++) //goes through children
            {
                Node * item = *i;
                int score = nodeScore(item, depth - 1); //finds score of child
                if(score > maxi) //if there's a better score
                {
                    maxi = score; //adjust score
                    node->setBestMove(item->getMoveFromParent()); //set the best move
                    node->setScore(maxi);
                    
                }
            }
            return maxi;
        }
        
    }
    else
    {
        nodes = generateChildren(node, opponent); //finds all the children of the node
        if(nodes.size() == 0)
        {
            return heuristic(node, 1);
        } 
        else
        {
            int mini = 10000000; //looking for the minimum score of the children nodes
            for(i = nodes.begin(); i != nodes.end(); i++) //goes through children
            {
                Node * item = *i;
                int score = nodeScore(item, depth - 1); //finds score of child
                if(score < mini) //if there's a better score
                {
                    mini = score; //adjust score
                    node->setBestMove(item->getMoveFromParent()); //set the best move for the opponent
                    node->setScore(mini);
                }
            }
            return mini;
        }
        
    }
    return 0;
}

